from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt

from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from rest_framework import status

from .models import Game
from .serializers import GameSerializer


class JSONResponse(HttpResponse):

    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)


@csrf_exempt
def game_list(request):
    if request.method == 'GET':
        games = Game.objects.all()
        games_serialized = GameSerializer(games, many=True)

        return JSONResponse(games_serialized.data)

    elif request.method == 'POST':
        parser = JSONParser()
        parsed_game = parser.parse(request)
        deserialized_game = GameSerializer(data=parsed_game)
        if deserialized_game.is_valid():
            deserialized_game.save()
            return JSONResponse(
                deserialized_game.data,
                status=status.HTTP_201_CREATED
            )

        else:
            return JSONResponse(
                deserialized_game.errors,
                status=status.HTTP_400_BAD_REQUEST
            )


@csrf_exempt
def game_detail(request, pk):
    try:
        game = Game.objects.get(pk=pk)
    except Game.DoesNotExist:
        return HttpResponse(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serialized_game = GameSerializer(game)
        return JSONResponse(serialized_game.data)

    elif request.method == 'PUT':
        game_data = JSONParser().parse(request)
        deserialized_game = GameSerializer(game, data=game_data)
        if deserialized_game.is_valid():
            deserialized_game.save()
            return JSONResponse(deserialized_game.data)
        else:
            return JSONResponse(
                deserialized_game.errors,
                status=status.HTTP_400_BAD_REQUEST
            )

    elif request.method == 'DELETE':
        game.delete()
        return HttpResponse(status=status.HTTP_204_NO_CONTENT)
